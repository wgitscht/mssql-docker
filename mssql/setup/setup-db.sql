IF NOT EXISTS (SELECT name FROM sys.databases WHERE name = 'dbi')
BEGIN
    CREATE DATABASE dbi;
END
GO

DECLARE @DBI_USER_PASSWORD NVARCHAR(100);
SET @DBI_USER_PASSWORD = '$(DBI_USER_PASSWORD)';

IF NOT EXISTS (SELECT name FROM sys.sql_logins WHERE name = 'dbi_user')
BEGIN
    USE dbi;
    PRINT 'dbi_user'
    PRINT @DBI_USER_PASSWORD

    DECLARE @sql NVARCHAR(MAX);
    SET @sql = 'CREATE LOGIN dbi_user WITH PASSWORD = ' + QUOTENAME(@DBI_USER_PASSWORD, '''') + ', CHECK_POLICY = OFF;';    
    EXEC sp_executesql @sql;

    CREATE USER dbi_user FOR LOGIN dbi_user WITH DEFAULT_SCHEMA = dbo;
    ALTER ROLE db_owner ADD MEMBER dbi_user;    
END
GO