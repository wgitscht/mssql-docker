## Dockerized Version of MSSQL Server
this image is intended for local testing only. 

### prerequisits
oocker desktop for windows, or docker for linux/macos is required
for windows install via winget

```winget install Docker.DockerDesktop```

for linux use your favorite package manager e.g.

```pacman -S docker```

### howto run
open a cmd shell and run. 

``docker-compose up``

For the first time setup, give it a minute to create the database and user, subsequent starts will be quicker.
The user and pwd for the login are printed on the console once the setup is finished.

if you don't want to see the output use 
``docker-compose up -d``
which is the more common way to start.

## how to connect with datagrip
Create a new connection

![new con](images/datagrip-new-con.png)

Configure it for your user, careful we run on port 1455!

![config](images/datagrip-config.png)


Create a schema and a table as we did in class.

### howto configure (advanced)
if you want to change the password check [docker-compose.yml] and change the DBI_USER_PASSWORD. 
Careful you need to do this **before** the first start. Afterwards you can still change
your password by logging in and issuing 

```sql
ALTER LOGIN dbi_user 
WITH PASSWORD = 'new_password';
```




